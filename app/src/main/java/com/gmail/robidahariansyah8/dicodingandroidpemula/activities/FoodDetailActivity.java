package com.gmail.robidahariansyah8.dicodingandroidpemula.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gmail.robidahariansyah8.dicodingandroidpemula.R;

public class FoodDetailActivity extends AppCompatActivity {

    public static final String EXTRA_IMAGE = "extra_image";
    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_DESC = "extra_desc";
    public static final String EXTRA_PRICE = "extra_price";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);
        //abc

        ImageView photo = null;
        photo = findViewById(R.id.item_image_detail);
        TextView tvName = findViewById(R.id.item_name_detail);
        TextView tvDesc = findViewById(R.id.item_desc_detail);
        TextView tvPrice = findViewById(R.id.item_price_detail);

        photo.setImageResource(getIntent().getIntExtra(EXTRA_IMAGE, 0));
        tvName.setText(getIntent().getStringExtra(EXTRA_NAME));
        tvDesc.setText(getIntent().getStringExtra(EXTRA_DESC));
        tvPrice.setText("$ " + (getIntent().getIntExtra(EXTRA_PRICE, 0)) + " USD");
    }
}
