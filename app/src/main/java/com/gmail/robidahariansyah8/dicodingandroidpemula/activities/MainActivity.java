package com.gmail.robidahariansyah8.dicodingandroidpemula.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.gmail.robidahariansyah8.dicodingandroidpemula.R;
import com.gmail.robidahariansyah8.dicodingandroidpemula.adapters.ListCardFoodAdapter;
import com.gmail.robidahariansyah8.dicodingandroidpemula.models.Food;
import com.gmail.robidahariansyah8.dicodingandroidpemula.models.FoodsData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvFoods;
    private ArrayList<Food> listFood = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvFoods = findViewById(R.id.rv_foods);
        rvFoods.setHasFixedSize(true);

        listFood.addAll(FoodsData.getListData());
        showRecylerList();
    }

    private void showSelectedFood(Food food) {
        // Toast.makeText(MainActivity.this, "Kamu Memilih : " + food.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this, FoodDetailActivity.class);
        intent.putExtra(FoodDetailActivity.EXTRA_IMAGE, food.getPhoto());
        intent.putExtra(FoodDetailActivity.EXTRA_NAME, food.getName());
        intent.putExtra(FoodDetailActivity.EXTRA_DESC, food.getDesc());
        intent.putExtra(FoodDetailActivity.EXTRA_PRICE, food.getPrice());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        setMode(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    private void setMode(int itemId) {
        switch (itemId) {
            case R.id.action_profile:
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                intent.putExtra(ProfileActivity.EXTRA_NAME, "Robi Dahariansyah");
                intent.putExtra(ProfileActivity.EXTRA_EMAIL, "robidahariansyah8@gmail.com");
                startActivity(intent);
                break;
        }
    }

    private void showRecylerList() {
        rvFoods.setLayoutManager(new LinearLayoutManager(this));
        ListCardFoodAdapter listCardFoodAdapter = new ListCardFoodAdapter(listFood);
        rvFoods.setAdapter(listCardFoodAdapter);

        listCardFoodAdapter.setOnItemClickCallback(new ListCardFoodAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Food food) {
                showSelectedFood(food);
            }
        });
    }
}
