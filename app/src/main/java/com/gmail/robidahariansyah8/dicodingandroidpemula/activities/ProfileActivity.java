package com.gmail.robidahariansyah8.dicodingandroidpemula.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.gmail.robidahariansyah8.dicodingandroidpemula.R;

public class ProfileActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_EMAIL = "extra_email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        TextView tvName = findViewById(R.id.tv_name);
        TextView tvEmail = findViewById(R.id.tv_email);

        tvName.setText(getIntent().getStringExtra(EXTRA_NAME));
        tvEmail.setText(getIntent().getStringExtra(EXTRA_EMAIL));
    }
}
