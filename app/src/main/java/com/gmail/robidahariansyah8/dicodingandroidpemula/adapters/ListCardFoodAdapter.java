package com.gmail.robidahariansyah8.dicodingandroidpemula.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gmail.robidahariansyah8.dicodingandroidpemula.R;
import com.gmail.robidahariansyah8.dicodingandroidpemula.models.Food;

import java.util.ArrayList;

public class ListCardFoodAdapter extends RecyclerView.Adapter<ListCardFoodAdapter.ListCardViewHolder> {
    private ArrayList<Food> listFood;

    public ListCardFoodAdapter(ArrayList<Food> listFood) {
        this.listFood = listFood;
    }

    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public interface OnItemClickCallback {
        void onItemClicked(Food food);
    }

    @NonNull
    @Override
    public ListCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListCardViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_foods, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ListCardViewHolder holder, int position) {
        Food food = listFood.get(position);
        Glide.with(holder.itemView.getContext())
                .load(food.getPhoto())
                .apply(new RequestOptions().override(200, 200))
                .into(holder.photo);

        holder.tvName.setText(food.getName());
        holder.tvDesc.setText(food.getDesc());
        holder.tvPrice.setText("$ " + String.valueOf(food.getPrice()) + " USD");

        // klik recyclerview
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listFood.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFood.size();
    }

    class ListCardViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView tvName, tvDesc, tvPrice;

        ListCardViewHolder(@NonNull View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.item_photo_listcard);
            tvName = itemView.findViewById(R.id.item_name_listcard);
            tvDesc = itemView.findViewById(R.id.item_desc_listcard);
            tvPrice = itemView.findViewById(R.id.item_price_listcard);
        }
    }
}
