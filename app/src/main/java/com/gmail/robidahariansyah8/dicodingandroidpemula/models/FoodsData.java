package com.gmail.robidahariansyah8.dicodingandroidpemula.models;

import com.gmail.robidahariansyah8.dicodingandroidpemula.R;

import java.util.ArrayList;

public class FoodsData {
    public static String[] foodNames = {
            "Nasi Goreng",
            "Sate",
            "Mie Goreng",
            "Rendang",
            "Gado-Gado",
            "Bakso",
            "Soto Ayam",
            "Mie Ayam",
            "Sayur Lodeh",
            "Nasi Kuning"
    };

    public static String[] foodDescs = {
            "Nasi goreng adalah sebuah makanan berupa nasi yang digoreng dan diaduk dalam minyak goreng atau margarin, biasanya ditambah kecap manis, bawang merah, bawang putih, asam jawa, lada dan bumbu-bumbu lainnya, seperti telur, ayam, dan kerupuk.",
            "Sate adalah makanan yang terbuat dari daging yang dipotong kecil-kecil dan ditusuk sedemikian rupa dengan tusukan lidi tulang daun kelapa atau bambu kemudian dipanggang menggunakan bara arang kayu. Sate disajikan dengan berbagai macam bumbu yang bergantung pada variasi resep sate.",
            "Mi goreng berarti \"mi yang digoreng\" adalah makanan yang populer dan digemari di Indonesia, Malaysia, dan Singapura.",
            "Rendang atau randang adalah masakan daging dengan bumbu rempah-rempah yang berasal dari Minangkabau. Masakan ini dihasilkan dari proses memasak yang dipanaskan berulang-ulang menggunakan santan sampai kuahnya kering sama sekali.",
            "Gado-gado adalah salah satu makanan yang berasal dari Indonesia yang berupa sayur-sayuran yang direbus dan dicampur jadi satu, dengan bumbu kacang atau saus dari kacang tanah dan yang dihaluskan disertai irisan telur dan pada umumnya banyak yang menambahkan kentang rebus yang sudah dihaluskan untuk saus gado gado kentang rebus dimasak bersamaan dengan bumbu kacang kemudian di atasnya ditaburi bawang goreng.",
            "Bakso atau baso adalah jenis bola daging yang lazim ditemukan pada masakan Indonesia. Bakso umumnya dibuat dari campuran daging sapi giling dan tepung tapioka, akan tetapi ada juga bakso yang terbuat dari daging ayam, ikan, atau udang bahkan daging kerbau.",
            "Soto ayam adalah makanan khas Indonesia yang berupa sejenis sup ayam dengan kuah yang berwarna kekuningan. Warna kuning ini dikarenakan oleh kunyit yang digunakan sebagai bumbu. Soto ayam banyak ditemukan di daerah-daerah di Indonesia dan Singapura.",
            "Mi ayam atau bakmi ayam adalah masakan Indonesia yang terbuat dari mi kuning direbus mendidih kemudian ditaburi saos kecap khusus beserta daging ayam dan sayuran. Mi Ayam terkadang ditambahi dengan bakso, pangsit dan jamur. Mi berasal dari Tiongkok tetapi mi ayam yang serupa di Indonesia tidak ditemukan di Tiongkok.",
            "Sayur lodeh adalah masakan sayur yang berkuah santan khas Indonesia, terutama di daerah Jawa Tengah. Sayur lodeh mempunyai berbagai macam variasi terutama pada bumbunya, ada yang santannya berwarna putih dan ada juga yang santannya berwarna kuning kemerahan.",
            "Nasi kuning adalah makanan khas Indonesia. Makanan ini terbuat dari beras yang dimasak bersama dengan kunyit serta santan dan rempah-rempah. Dengan ditambahkannya bumbu-bumbu dan santan, nasi kuning memiliki rasa yang lebih gurih daripada nasi putih."
    };

    public static int[] foodImages = {
            R.drawable.nasi_goreng,
            R.drawable.sate,
            R.drawable.mie_goreng,
            R.drawable.rendang,
            R.drawable.gado_gado,
            R.drawable.bakso,
            R.drawable.soto_ayam,
            R.drawable.mie_ayam,
            R.drawable.sayur_lodeh,
            R.drawable.nasi_kuning
    };

    public static int[] foodPrices = {
            230,
            455,
            890,
            787,
            887,
            434,
            436,
            221,
            443,
            667
    };

    public static ArrayList<Food> getListData() {
        ArrayList<Food> listFood = new ArrayList<>();
        for (int i = 0; i < foodNames.length; i++) {
            Food food = new Food();
            food.setName(foodNames[i]);
            food.setDesc(foodDescs[i]);
            food.setPrice(foodPrices[i]);
            food.setPhoto(foodImages[i]);
            listFood.add(food);
        }

        return listFood;
    }
}
